
import java.io.*;
class Number{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number:");
		int num=Integer.parseInt(br.readLine());
		int temp=num;
		int i=1;
		while(num>1){
			i=num*i;
			num--;
		}
		System.out.println("Factorial of "+temp+" is "+i);
	}
}
