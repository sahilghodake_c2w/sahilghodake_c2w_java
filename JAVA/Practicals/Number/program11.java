
import java.io.*;
class Number{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number:");
		int num=Integer.parseInt(br.readLine());
		int temp=num;
		int val=0;
		while(num>0){
			int rem=num%10;
			val=val*10+rem;
			num/=10;
		}
		if(temp==val){
			System.out.println(temp+" is a palindrome number");
		}
		else{
			System.out.println(temp+" is not a palindrome number");
	}
}
}
