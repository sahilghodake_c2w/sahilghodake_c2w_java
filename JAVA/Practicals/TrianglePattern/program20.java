import java.io.*;

class Triangle20{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Rows:");
		int row =Integer.parseInt(br.readLine());
		char ch='a';
		int num=1;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				if(i%2==1){
				System.out.print(num+" ");
			}
			else{
				System.out.print((char)ch+" ");
				
		        }
			num++;
			ch++;
			}
			System.out.println();
		
		}
	}
}


