class WhileDemo{
	        public static void main(String[] args){
			int num=256985;
			int prod=1;
		        while(num>0){
				int rem=num%10;
				if(rem%2==1){
					prod=prod*rem;
				}
			        num/=10;
			}

		        System.out.println("Product of Odd Digits:"+prod);
			
		        

		}
}

