class WhileDemo{
	public static void main(String[] args){
		long num=9307922405L;
		long sum=0L;
		while(num>0){
			long rem=num%10;
			sum=sum+rem;
			num/=10;
		}
		System.out.println("Sum of Digits is:"+sum);
		

	}
}
