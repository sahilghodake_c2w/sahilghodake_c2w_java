import java.io.*;
class InputDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Company Name:");
		String CmpName=br.readLine();
		System.out.print("Enter Employee Name:");
		String empName=br.readLine();
		System.out.print("Enter empl id:");
		int empId=Integer.parseInt(br.readLine());
		System.out.print("Enter empl Salary:");
		double sal=Double.parseDouble(br.readLine());
		System.out.println("Company Name:"+CmpName);
		System.out.println("Employee Name:"+empName);
		System.out.println("Employee Id:"+empId);
		System.out.println("Employee Salary:"+sal);

	}
}
