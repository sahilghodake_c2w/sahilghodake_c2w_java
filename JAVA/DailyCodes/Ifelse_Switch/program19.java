class SwitchDemo11{
	public static void main(String[] args){
		String friends="Niraj";
		System.out.println("Before Switch");
                switch(friends){
			case "Kunal":
				System.out.println("Barclays");
				break;
			case "Rutik":
				System.out.println("BMC Software");
				break;
			case "Niraj":
				System.out.println("IBM");
				break;
			default:
				System.out.println("In Default state");
		}
		System.out.println("After Switch");
	}
}
	 		 

