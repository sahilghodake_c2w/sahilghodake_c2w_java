
import java.util.Scanner;
class Space{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter rows:");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			for(int space=1;space<i;space++){
				System.out.print("	");
			}
			int num=i;
			for(int j=row;j>=i;j--){
				System.out.print(num+"	");
				num++;
			}

		System.out.println();
		}
	}
}
