
import java.util.Scanner;
class Space{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter rows:");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			for(int space=1;space<=row-i;space++){
				System.out.print("	");
			}
			int num=1;
			for(int j=1;j<=i;j++){
				System.out.print(num+"	");
				num++;
			}
		System.out.println();
		}
	}
}
