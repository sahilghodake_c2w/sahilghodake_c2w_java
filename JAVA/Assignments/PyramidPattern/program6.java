import java.util.*;
class Pyramid{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter rows:");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			for(int sp=row;sp>i;sp--){
				System.out.print("	");
			}
			int num=row;
			for(int j=1;j<=i*2-1;j++){
				if(j<i){
					System.out.print(num-- +"	");
				}
				else{
					System.out.print(num++ +"	");
				}
			}
			System.out.println();
		}
	}
}

