import java.util.*;
class Pyramid{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter rows:");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			for(int sp=row;sp>i;sp--){
				System.out.print("	");
			}
			int ch=row-i+1;
			int num=ch+64;
			for(int j=1;j<=i*2-1;j++){
				if(j<i){
					System.out.print((char)num++ +"	");
				}
				else{
					System.out.print((char)num-- +"	");
				}
			}
			System.out.println();
		}
	}
}

