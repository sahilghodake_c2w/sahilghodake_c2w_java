import java.util.*;
class Pyramid{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter rows:");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			for(int sp=row;sp>i;sp--){
				System.out.print("	");
			}
			int num=1;
			for(int j=1;j<=i*2-1;j++){
				if(i%2==1){
					if(i>j){
				System.out.print((char)(num+64)+"	");
				num++;
					}
					else{
						System.out.print((char)(num+64)+"	");
						num--;
					}
				}
				else{
					if(i>j){
				System.out.print((char)(num+96)+"	");
				num++;
					}
					else{
						System.out.print((char)(num+96)+"	");
						num--;
					}
				}
				}
				System.out.println();
	}
}
}

