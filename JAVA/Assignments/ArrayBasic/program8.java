
import java.io.*;
class BasicArray{
	public static void main(String[] args)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of employees:");
		int numEmp=Integer.parseInt(br.readLine());
		int age[]=new int[numEmp];
		for(int i=0;i<numEmp;i++){
			System.out.print("Enter age of employee "+(i+1)+":");
			age[i]=Integer.parseInt(br.readLine());
		}
		for(int i=0;i<numEmp;i++){
			System.out.println("Age of Employee "+(i+1)+":"+age[i]);
		}
		}
		
}



