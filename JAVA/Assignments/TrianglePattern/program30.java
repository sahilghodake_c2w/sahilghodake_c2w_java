
import java.util.*;
class Triangle{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter no of Rows:");
		int row=sc.nextInt();
		int num=row;
		for(int i=row;i>=1;i--){
			for(int j=i;j>=1;j--){
				if(i%2==0){
					System.out.print((char)(64+j)+" ");
				}
				else{
					System.out.print((char)(96+j)+" ");
				}
			}
		System.out.println();
	}
}
}

