class SwitchDemo{
	public static void main(String[] args){
		String size="U";
		switch(size){
			case "S":
				System.out.println("Small");
				break;
			case "M":
				System.out.println("Medium");
				break;
			case "L":
			        System.out.println("Large");
				break;
			case "XL":
				System.out.println("Extra large");
				break;
			//case 'F':
			  //      System.out.println("Fail");
			default:
				System.out.println("Invalid size");
				break;
		}
	}
}
