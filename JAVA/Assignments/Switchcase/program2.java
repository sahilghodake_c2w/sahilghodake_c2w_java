class SwitchDemo{
	public static void main(String[] args){
		char data='O';
		switch(data){
			case 'O':
				System.out.println("Outstanding");
				break;
			case 'A':
				System.out.println("Excellent");
				break;
			case 'B':
			        System.out.println("Very Good");
				break;
			case 'P':
				System.out.println("Pass");
				break;
			case 'F':
			        System.out.println("Fail");
			default:
				System.out.println("Invalid grade");
				break;
		}
	}
}
