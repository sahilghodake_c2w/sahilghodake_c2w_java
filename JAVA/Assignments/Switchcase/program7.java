class SwitchDemo{
	public static void main(String[] args){
		int budget=200;
		switch(budget){
			case 15000:
				System.out.println("Destination is Jammu & Kashmir");
				break;
			case 10000:
				System.out.println("Destination is Manali");
				break;
			case 6000:
			        System.out.println("Destination is Amritsar");
				break;
			case 2000:
				System.out.println("Destination is Mahabaleshwar");
				break;
			default:
				System.out.println("Try next time");
				
		}
	}
}
