class SwitchDemo{
	public static void main(String[] args){
		int num=28;
		int rem=num%2;
		switch(rem){
			case 0:
				System.out.println(num+" is an even number");
				break;
			case 1:
				System.out.println(num+" is an odd number");
				break;
		}
	}
}
